package top.hmtools.uri;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;

public class UriBuilderTest {

	/**
	 * 测试在URL后面补充参数
	 */
	@Test
	public void uriBuilder(){
		String url = "http://www.hm.hn.cn/hello?accesstoken=aaaaaa";
		
		Map<String,String> params = new HashMap<String, String>();
		params.put("bb", "bbbbbbbbb");
		params.put("cc", "ccccc");
		
		String generateUrl = this.generateUrl(url, params);
		System.out.println(generateUrl);
	}
	
	private String generateUrl(String oldUrl,Map<String,String> params){
		try {
			URIBuilder builder = new URIBuilder(oldUrl);
			if(params != null && !params.isEmpty()){
				Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
				while(iterator.hasNext()){
					Entry<String, String> item = iterator.next();
					
					builder.addParameter(item.getKey(), item.getValue().toString());
				}
			}
			return builder.build().toURL().toString();
		} catch (URISyntaxException | MalformedURLException e) {
			throw new RuntimeException("http get请求异常：",e);
		}
	}
}
