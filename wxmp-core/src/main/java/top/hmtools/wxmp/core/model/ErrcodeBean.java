package top.hmtools.wxmp.core.model;

/**
 * 微信返回的统一错误代码实体类
 * @author Hybomyth
 *
 */
public class ErrcodeBean {

	/**
	 * 异常代码
	 */
	protected int errcode;
	
	/**
	 * 异常信息
	 */
	protected String errmsg;

	public int getErrcode() {
		return errcode;
	}

	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	@Override
	public String toString() {
		return "ErrcodeBean [errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
