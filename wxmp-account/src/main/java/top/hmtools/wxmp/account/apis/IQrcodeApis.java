package top.hmtools.wxmp.account.apis;

import java.io.InputStream;

import top.hmtools.wxmp.account.models.QRCodeParam;
import top.hmtools.wxmp.account.models.QRCodeResult;
import top.hmtools.wxmp.account.models.TicketParam;
import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;

@WxmpMapper
public interface IQrcodeApis {

	/**
	 * 创建二维码ticket
	 * @param codeParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/qrcode/create")
	public QRCodeResult create(QRCodeParam codeParam);
	
	/**
	 * 根据ticket获取二维码图片
	 * @param ticketParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="https://mp.weixin.qq.com/cgi-bin/showqrcode")
	public InputStream showQrCode(TicketParam ticketParam);
}
