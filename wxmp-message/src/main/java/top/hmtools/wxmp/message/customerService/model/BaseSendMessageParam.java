package top.hmtools.wxmp.message.customerService.model;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

import com.alibaba.fastjson.JSON;

import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 客服接口-发消息 - 基本消息
 * @author HyboWork
 *
 */
public class BaseSendMessageParam {

	/**
	 * 接收消息的用户openID
	 */
	protected String touser;
	
	/**
	 * 消息类型
	 */
	protected MsgType msgtype;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public MsgType getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(MsgType msgtype) {
		this.msgtype = msgtype;
	}

	@Override
	public String toString() {
		return "BaseCustomerServiceMessage [touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	/**
	 * 从json字符串中提取数据到本对象实例
	 * @param jsonStr
	 */
	public void loadDataFromJsonString(String jsonStr){
		if(jsonStr == null || jsonStr.length()<1){
			return;
		}
		
		Object fromJson = JSON.parse(jsonStr);
		if(fromJson == null){
			return;
		}
		
		try {
			BeanUtils.copyProperties(fromJson, this);
		} catch (BeansException e) {
			throw new IllegalArgumentException("从Json字符串中读取数据到本实体类失败", e);
		}
	}
	
	/**
	 * 转换成json字符串
	 * @return
	 */
	public String toJsonStr(){
		return JSON.toJSONString(this);
	}
}
