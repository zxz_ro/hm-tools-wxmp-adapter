package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * Auto-generated: 2019-08-25 15:41:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Music {

    private String title;
    private String description;
    private String musicurl;
    private String hqmusicurl;
    private String thumb_media_id;
    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setMusicurl(String musicurl) {
         this.musicurl = musicurl;
     }
     public String getMusicurl() {
         return musicurl;
     }

    public void setHqmusicurl(String hqmusicurl) {
         this.hqmusicurl = hqmusicurl;
     }
     public String getHqmusicurl() {
         return hqmusicurl;
     }

    public void setThumb_media_id(String thumb_media_id) {
         this.thumb_media_id = thumb_media_id;
     }
     public String getThumb_media_id() {
         return thumb_media_id;
     }

}