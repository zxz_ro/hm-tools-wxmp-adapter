package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * Auto-generated: 2019-08-25 15:37:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Video {

    private String media_id;
    private String thumb_media_id;
    private String title;
    private String description;
    public void setMedia_id(String media_id) {
         this.media_id = media_id;
     }
     public String getMedia_id() {
         return media_id;
     }

    public void setThumb_media_id(String thumb_media_id) {
         this.thumb_media_id = thumb_media_id;
     }
     public String getThumb_media_id() {
         return thumb_media_id;
     }

    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

}