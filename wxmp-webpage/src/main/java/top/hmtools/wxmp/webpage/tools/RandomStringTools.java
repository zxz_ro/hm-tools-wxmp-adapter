package top.hmtools.wxmp.webpage.tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 获取随机的字符串数据
 * 
 * @author Hybomyth 创建日期：2016-12-11下午8:58:45
 */
public class RandomStringTools {
    
    protected static Logger logger = LoggerFactory.getLogger(RandomStringTools.class);

	/**
	 * 字典
	 */
//	public static String[] CHARS = new String[] { "a", "b", "c", "d", "e", "f",
//			"g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
//			"t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
//			"6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
//			"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
//			"W", "X", "Y", "Z" };

	/**
	 * 获取指定长度，指定字典的随机字符串
	 * @param length	指定随机字符串的长度
	 * @param dictories	随机字符串取值的字典
	 * @return
	 */
	public static String getGenerateShortUuid(int length, char[] dictories) {
		StringBuffer shortBuffer = new StringBuffer();

		// 取uuid值，并去掉其中的“-”
		int uuidTimes = length / 8;
		String uuid = null;
		StringBuffer sb_uuid = new StringBuffer();
		for (int ii = 0; ii < uuidTimes + 1; ii++) {
			sb_uuid.append(UUID.randomUUID().toString().replace("-", ""));
		}
		uuid = sb_uuid.toString();

		// 通过uuid获取指针，并从chars字典中获取值，并拼接成随机字符串
		for (int ii = 0; ii < length; ii++) {
			String str = uuid.substring(ii * 4, ii * 4 + 4);
			// 将16进制的uuid片段转换成10进制
			int x = Integer.parseInt(str, 16);
			int index = x % 0x3E;
			shortBuffer.append(dictories[index]);
		}
	    logger.debug("dictories:"+new String(dictories));
		logger.debug("uuid:"+uuid);
		logger.debug("ShortUuid:"+shortBuffer.toString());
		return shortBuffer.toString();
	}

	/**
	 * 获取指定长度的随机字符串 <br>
	 * 仅数字跟字母组合，区分大小写。 <br>
	 * 可生成指定任意长度
	 * 
	 * @param length
	 * @return
	 */
	public static String getGenerateShortUuid(int length) {
		char[] dics = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		return getGenerateShortUuid(length,dics);
	}

	/**
	 * 获取指定前缀+指定长度的随机字符串
	 * 
	 * @param prefix
	 * @param length
	 * @return
	 */
	public static String getPrefixShortUuid(String prefix, int length) {
		return prefix + getGenerateShortUuid(length);
	}

	/**
	 * 获取当前年份前缀+指定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getYyyyPrefixShortUuid(int length) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		return getSdfPrefixShortUuid(sdf, length);
	}

	/**
	 * 获取当前时间 yyyyMMddHHmmssSSS前缀（17位长度）+指定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getYyyyMMddHHmmssSSSPrifixShortUuid(int length) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return getSdfPrefixShortUuid(sdf, length);
	}

	/**
	 * 获取当前时间 yyyyMMddHHmmss前缀+指定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getYyyyMMddHHmmssPrifixShortUuid(int length) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return getSdfPrefixShortUuid(sdf, length);
	}

	/**
	 * 获取当前时间 yyyyMMdd前缀+指定长度的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String getYyyyMMddPrifixShortUuid(int length) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return getSdfPrefixShortUuid(sdf, length);
	}

	/**
	 * 获取日期格式化为前缀的+指定随机长度的随机字符串
	 * 
	 * @param sdf
	 * @param length
	 * @return
	 */
	public static String getSdfPrefixShortUuid(SimpleDateFormat sdf, int length) {
		String formatDateStr = sdf.format(new Date());
		return getPrefixShortUuid(formatDateStr, length);
	}
	
	/**
	 * 获取当前时间 yyyyMMddHHmmssSSS前缀（17位长度）+指定范围的随机整数
	* 方法说明：                    getYyyyMMddHHmmssSSSPrifixIntegerRange
	* 输入参数说明：           @param min
	* 输入参数说明：           @param max
	* 输入参数说明：           @return
	* 输出参数说明：           String
	*
	*
	 */
	public static String getYyyyMMddHHmmssSSSPrifixIntegerRange(int min,int max){
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String formatDateStr = sdf.format(new Date());
        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
        return formatDateStr+s;
	}
	
	/**
	 * 获取当前时间 yyyyMMddHHmmssSSS前缀（17位长度）+指定位数的随机整数
	* 方法说明：                    getYyyyMMddHHmmssSSSPrifixInteger
	* 输入参数说明：           @param length
	* 输入参数说明：           @return
	* 输出参数说明：           String
	*
	*
	 */
	public static String getYyyyMMddHHmmssSSSPrifixInteger(int length){
	    String shortNOs = getRandomNumber(length);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String formatDateStr = sdf.format(new Date());
	    return formatDateStr+shortNOs;
	}
	
	/**
	 * 生成指定位数的随机整数
	* 方法说明：                    getRandomNumber
	* 输入参数说明：           @param length
	* 输入参数说明：           @return
	* 输出参数说明：           String
	*
	*
	 */
	public static String getRandomNumber(int length){
	    return getGenerateShortUuid(length, "01234567890123456789012345678901234567890123456789012345678901".toCharArray());
	}
	
	
	
}
