package top.hmtools.wxmp.comment.models;

public class OpenCommentParam {

	/**
	 * 群发返回的msg_data_id
	 */
	private Long msg_data_id;
	
	/**
	 * 多图文时，用来指定第几篇图文，从0开始，不带默认操作该msg_data_id的第一篇图文
	 */
	private Long index;

	public Long getMsg_data_id() {
		return msg_data_id;
	}

	/**
	 * 群发返回的msg_data_id
	 * @param msg_data_id
	 */
	public void setMsg_data_id(Long msg_data_id) {
		this.msg_data_id = msg_data_id;
	}

	public Long getIndex() {
		return index;
	}

	/**
	 * 多图文时，用来指定第几篇图文，从0开始，不带默认操作该msg_data_id的第一篇图文
	 * @param index
	 */
	public void setIndex(Long index) {
		this.index = index;
	}
	
	
}
